package pos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pos.models.Hotel;
import pos.models.PointOfSale;
import pos.models.User;
import pos.repository.HotelRepository;
import pos.repository.PointOfSaleRepository;

import java.util.List;

/**
 * Created by utsav on 24/8/16.
 */
@Service
public class PointOfSaleService {

    @Autowired
    private PointOfSaleRepository pointOfSaleRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private HotelService hotelService;

    public PointOfSale fetchById(Long id){
        return pointOfSaleRepository.findOne(id);
    }

    public PointOfSale create(PointOfSale pointOfSale){
        return pointOfSaleRepository.save(pointOfSale);
    }

    public PointOfSale update(PointOfSale pointOfSale){
        return pointOfSaleRepository.save(pointOfSale);
    }

    public List<PointOfSale> findByUserAndHotel(Long userId, Long hotelId){
        return pointOfSaleRepository.findByHotelAndUser(hotelService.fetchById(hotelId), userService.fetchById(userId));
    }
}
