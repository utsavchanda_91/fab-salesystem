package pos.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pos.models.User;
import pos.repository.UserRepository;

/**
 * Created by Utsav Chanda on 8/28/2016.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User fetchById(Long id){
        return userRepository.findOne(id);
    }

    public User create(User user){
        return userRepository.save(user);
    }

    public User update(User user){
        return userRepository.save(user);
    }


}
