package pos.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pos.models.User;
import pos.models.UserDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import pos.services.UserService;

@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

  @Autowired
  private UserService userService;

  public User fetchById(@PathVariable Long id){
    return userService.fetchById(id);
  }

  @RequestMapping(method = RequestMethod.POST)
  public User create(@RequestBody User hotel){
    return userService.create(hotel);
  }

  @RequestMapping(method = RequestMethod.PUT)
  public User update(@RequestBody User hotel){
    return userService.update(hotel);
  }

  /*@Autowired
  private UserDao userDao;

  @RequestMapping(method = RequestMethod.POST)
  public String create(User user) {
    try {
      userDao.create(user);
    }
    catch (Exception ex) {
      return "Error creating the user: " + ex.toString();
    }
    return "User succesfully created!";
  }
  
  *//**
   * Delete the user with the passed id.
   *//*
  @RequestMapping(method = RequestMethod.DELETE)
  public String delete(User user) {
    try {
      userDao.delete(user);
    }
    catch (Exception ex) {
      return "Error deleting the user: " + ex.toString();
    }
    return "User succesfully deleted!";
  }
  
  *//**
   * Retrieve the id for the user with the passed email address.
   *//*
  @RequestMapping()
  public String getByEmail(String email) {
    String userId;
    try {
      User user = userDao.getByEmail(email);
      userId = String.valueOf(user.getId());
    }
    catch (Exception ex) {
      return "User not found: " + ex.toString();
    }
    return "The user id is: " + userId;
  }

  @RequestMapping(method = RequestMethod.PUT)
  public String updateName(User user) {
    try {
      userDao.update(user);
    }
    catch (Exception ex) {
      return "Error updating the user: " + ex.toString();
    }
    return "User succesfully updated!";
  }*/
  
}
