package pos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pos.models.Hotel;
import pos.models.PointOfSale;
import pos.services.HotelService;
import pos.services.PointOfSaleService;

import java.util.List;

/**
 * Created by utsav on 24/8/16.
 */
@RestController
@RequestMapping("/sales")
public class PointOfSaleController {

    @Autowired
    private PointOfSaleService pointOfSaleService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public PointOfSale fetchById(@PathVariable Long id){
        return pointOfSaleService.fetchById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public PointOfSale create(@RequestBody PointOfSale pointOfSale){

        return pointOfSaleService.create(pointOfSale);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public PointOfSale update(@RequestBody PointOfSale pointOfSale){
        return pointOfSaleService.update(pointOfSale);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<PointOfSale> fetchById(@RequestParam Long userId, @RequestParam Long hotelId){
        return pointOfSaleService.findByUserAndHotel(userId, hotelId);
    }


}
