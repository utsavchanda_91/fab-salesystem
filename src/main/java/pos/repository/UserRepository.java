package pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pos.models.Hotel;
import pos.models.User;

/**
 * Created by utsav on 24/8/16.
 */
public interface UserRepository extends JpaRepository<User, Long> {

}
