package pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pos.models.Hotel;
import pos.models.PointOfSale;
import pos.models.User;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * Created by utsav on 24/8/16.
 */
public interface PointOfSaleRepository extends JpaRepository<PointOfSale, Long> {

    List<PointOfSale> findByHotelAndUser(Hotel hotel, User user);

}
