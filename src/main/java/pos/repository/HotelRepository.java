package pos.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pos.models.Hotel;

/**
 * Created by utsav on 24/8/16.
 */
public interface HotelRepository extends JpaRepository<Hotel, Long> {

}
