package pos.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "HOTEL")
public class Hotel {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotNull
  private String email;

  @NotNull
  private String name;

  public long getId() {
    return id;
  }

  public void setId(long value) {
    this.id = value;
  }

  public String getEmail() {
    return email;
  }
  
  public void setEmail(String value) {
    this.email = value;
  }
  
  public String getName() {
    return name;
  }

  public void setName(String value) {
    this.name = value;
  }
  
}
