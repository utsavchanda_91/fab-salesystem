package pos.models;

/**
 * Created by utsav on 24/8/16.
 */
public enum Type {

    RESTAURANT("Restaurant"),
    BAKERY_SHOP("BakeryShop"),
    TRAVEL_DESK("TravelDesk")
    ;

    private String description;

    Type(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
