package pos.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "POINT_OF_SALE")
public class PointOfSale {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotNull
  @Enumerated(EnumType.STRING)
  private Type name;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "USER_ID")
  private User user;

  @NotNull
  @ManyToOne
  @JoinColumn(name = "HOTEL_ID")
  private Hotel hotel;

  @NotNull
  private Double sale;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Type getName() {
    return name;
  }

  public void setName(Type name) {
    this.name = name;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Hotel getHotel() {
    return hotel;
  }

  public void setHotel(Hotel hotel) {
    this.hotel = hotel;
  }

  public Double getSale() {
    return sale;
  }

  public void setSale(Double sale) {
    this.sale = sale;
  }
}