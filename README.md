# README #

Simple bootstrap project 

### How do I get set up? ###

Enter the db details in application.properties file

From mysql command line, execute the following query

create database salessystem;

**Commands to run** 

1. mvn clean install
2. mvn spring-boot:run -Dserver.port=8090

**POST call to create user**

http://localhost:8090/users

Request json
{
"name":"Utsav",
"email":"utsa@gmai.com"
}

**POST call to create hotels**

http://localhost:8090/hotels

Request json
{
"name":"Radisson",
"email":"rad@gmai.com"
}

**POST call to enter sales data**

http://localhost:8090/sales

Request json
{
	"name":"RESTAURANT",
	"user":{"id":1},
	"hotel":{"id":1},
	"sale":100.0
}

**GET call to fetch sales data for particular user for a particular hotel**

http://localhost:8090/sales?userId=1&hotelId=1

Response // List of point of sales objects

[
    {
        "id": 1,
        "name": "RESTAURANT",
        "user": {
            "id": 1,
            "email": "utsa@gmai.com",
            "name": "Utsav"
        },
        "hotel": {
            "id": 1,
            "email": "rad@gmai.com",
            "name": "Radisson"
        },
        "sale": 100
    }
]